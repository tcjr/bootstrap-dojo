define([
  './support/with-node-data',
  './support/node-persist',
  './support/transition',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/ready',
  'dojo/_base/window',
  'dojo/on',
  'dojo/query',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/NodeList-traverse',
  'dojo/NodeList-dom',
  'dojo/NodeList-data',
  'dojo/NodeList-manipulate'
],
function(_WithNodeData, NodePersist, trans, declare, lang, ready, win, on, query, domAttr, domClass){
  
  /* TAB CLASS DEFINITION
   * ==================== */
   
   var Tab = declare([_WithNodeData], {
     constructor: function(element, options){
       this.domNode = element;
       lang.mixin(this, options || {});
     },
     
     show: function(){
       var $ul = query(this.domNode).closest('ul:not(.dropdown-menu)');
       var selector = this.getNodeData('target');
       
       if(!selector){
         selector = domAttr.get(this.domNode, 'href');
         selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
       }
       
       if(domClass.contains(query(this.domNode).parent('li')[0], 'active')){
         return
       }
       
       var previous = query('.active a', $ul[0]).last()[0];
       
       on.emit(this.domNode, 'show', {
         bubbles: true, //??? 
         cancelable: true, //???
         relatedTarget: previous
       })
       
       var $target = query(selector); // TODO: should we check for '' and '#'?
       
       this.activate(query(this.domNode).parent('li')[0], $ul[0]);
       
       this.activate($target[0], $target.parent()[0], lang.hitch(this, function(){
         on.emit(this.domNode, 'shown', {
           relatedTarget: previous
         })
       }))

     },
     
     
     activate: function(node, container, callback){
       //console.debug("ACTIVATE! node=%o; container=%o", node, container)
       
       var active = query('> .active', container)[0];
       var transition = callback && trans && domClass.contains(active, 'fade');

       function next(){
         domClass.remove(active, 'active');
         query('> .dropdonw-menu > .active', active).removeClass('active');

         domClass.add(node, 'active');

         if(transition){
           node.offsetWidth; //reflow?
           domClass.add(node, 'in');
         }else{
           domClass.remove(node, 'fade'); // will emit trans.end when complete
         }

         if(query(node).parent('.dropdown-menu')[0]){
           query(node).closest('li.dropdown').addClass('active');
         }

         if(callback){
           callback();
         }
       }
       
       
       
       if(transition){
         on.once(active, trans.end, next);
       }else{
         next();
       }
             
       domClass.remove(active, 'in');
     }
   });
   
   
   /* TAB PLUGIN DEFINITION
    * ===================== */

    lang.extend(query.NodeList, {
      tab: function(option){

        return this.forEach(function(node){

          var options = (typeof option == 'object') ? option : options

          var obj = NodePersist.loadFromNode(node, 'tab');
          if(!obj){
            obj = new Tab(node, options);
            NodePersist.saveToNode(obj, node, 'tab');
          }else{
            lang.mixin(obj, options)
          }

          if(typeof option == 'string'){
            obj[option]();
          }

        });

      }
    });



   ready(function () {
     on(win.body(), on.selector('[data-toggle="tab"], [data-toggle="pill"]', 'click'), function(e){
       e.preventDefault();
       query(this).tab('show')
     });
   });


   return Tab;
});
   
   