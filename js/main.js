define([
  './alert',
  './button',
  './dropdown',
  './modal',
  './popover',
  './tab',
  './tooltip'], 
function(Alert, Button, Dropdown, Modal, Popover, Tab, Tooltip){
  return {
    alert: Alert,
    button: Button,
    dropdown: Dropdown,
    modal: Modal,
    popover: Popover,
    tab: Tab,
    tooltip: Tooltip
  };
});