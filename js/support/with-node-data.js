define(['dojo/_base/declare', 'dojo/dom-attr', 'dojo/NodeList-data'], function(declare, domAttr){
  
  // Mixin to add accessors for retrieving and setting data in an object's domNode.
  // For set, the data will be saved to the node data cache using dojo's data api.  For retrieval, 
  // the data is loaded first from the node data cache.  If it is not found, it will check an
  // attribute on the node.
  
  var WND = declare([], {
    // TODO: verify this will return a falsy value when the key is not there
    getNodeData: function(key){
      return dojo._nodeData(this.domNode, key) || domAttr.get(this.domNode, 'data-'+key);
    },
    
    setNodeData: function(key, value){
      return dojo._nodeData(this.domNode, key, value);
    },
    
    removeNodeData: function(key){
      dojo._removeNodeData(this.domNode, key);
      domAttr.remove(this.domNode, 'data-'+key); // test this
    }
  });
  
  return WND;
});
