define(['dojo/NodeList-data'], 
function(){
  // Uses dojo's node data cache api to persist an object to a dom node
  
  // NOTE: dojo/NodeList-data updates the global dojo object
  
  return {
    loadFromNode: function(node, type){
      return dojo._nodeData(node, 'np-'+type);
    },

    saveToNode: function(obj, node, type){
      dojo._nodeData(node, 'np-'+type, obj)
    }
  };
  
});

