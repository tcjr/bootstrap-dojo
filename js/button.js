define([
  'dojo/_base/declare',
  'dojo/ready',
  'dojo/_base/window',
  'dojo/on',
  'dojo/query',
  'dojo/_base/lang',
  'dojo/dom-class',
  'dojo/dom-attr',
  './support/with-node-data',
  './support/node-persist',
  'dojo/NodeList-dom',
  'dojo/NodeList-manipulate',
  'dojo/NodeList-traverse'
],
function(declare, ready, win, on, query, lang, domClass, domAttr, _WithNodeData, NodePersist){
  
  var Button = declare([_WithNodeData], {
    'loading-text': 'loading...',
    
    constructor: function(element, options){
      this.domNode = element;
      lang.mixin(this, options);
    },
    
    setState: function(state){
      var d = 'disabled';
      
      if(!this.getNodeData('reset-text')){
        this.setNodeData('reset-text', this._getVal());
      }

      state = state + '-text';
      
      this._setVal(this.getNodeData(state) || this[state]);
      
      // push to event loop to allow forms to submit
      setTimeout(lang.hitch(this, function(){
        if(state == 'loading-text'){
          domClass.add(this.domNode, d);
          domAttr.set(this.domNode, d, d);
        }else{
          domClass.remove(this.domNode, d);
          domAttr.remove(this.domNode, d);
        }
      }), 0);
      
    },
    
    toggle: function(){
      
      var $parent = query(this.domNode).parents('[data-toggle="buttons-radio"]');
      if($parent.length){
        query('.active', $parent[0]).removeClass('active');
      }
        
      domClass.toggle(this.domNode, 'active');
    },
  
    _getVal: function(){
      // TODO: if the tag is INPUT, then use value, otherwise use innerHTML
      return this.domNode.innerHTML;
    },
    
    _setVal: function(value){
      this.domNode.innerHTML = value;
    }
    
  });


  /* BUTTON PLUGIN DEFINITION
   * ======================== */
   
  lang.extend(query.NodeList, {
    button: function(option){
      
      return this.forEach(function(node){
        
        var options = (typeof option == 'object') ? option : options
        
        var obj = NodePersist.loadFromNode(node, 'button');
        if(!obj){
          obj = new Button(node, options);
          NodePersist.saveToNode(obj, node, 'button');
        }
        //lang.mixin(obj, options)
        
        if(option == 'toggle'){
          obj.toggle();
        }else if(option){
          obj.setState(option)
        }
      
      });
      
    }
  });
  
  
  /* BUTTON DATA-API
   * =============== */

    ready(function() {
      on(win.body(), '[data-toggle^=button] .btn:click, [data-toggle^=button].btn:click', function(e){
        var $btn = query(e.target);
        $btn.button('toggle');
      });
    });
  
});
