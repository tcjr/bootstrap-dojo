define([
  './support/with-node-data',
  './support/node-persist',
  'dojo/ready',
  'dojo/_base/window',
  'dojo/on',
  'dojo/query',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/NodeList-data',
  'dojo/NodeList-dom',
  'dojo/NodeList-manipulate',
  'dojo/NodeList-traverse'
],
function(_WithNodeData, NodePersist, ready, win, on, query, declare, lang, domAttr, domClass){
  var toggle = '[data-toggle="dropdown"]';
  
  var Dropdown = declare([_WithNodeData], {
    
    constructor: function(element, options){
      //console.debug("CTOR; args=%o", arguments);
      this.domNode = element;
      lang.mixin(this, options);
    },
    
    toggle: function(e){
      console.debug("toggling! e=%o; this=%o",e,this);

      var selector = this.getNodeData('target');

      if(!selector){
        selector = domAttr.get(this.domNode, 'href');
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '')
      }
      console.debug("selector is %o", selector);

      // NOTE: Dojo's selector returns non empty results for '#' and ''
      if(!selector || selector=='#' || selector==''){ 
        //targetNode = this.domNode.parentNode;
        targetNode = query(this.domNode).closest('.dropdown')[0];
      }else{
        targetNode = query(selector)[0];
        if(!targetNode){
          //targetNode = this.domNode.parentNode;
          targetNode = query(this.domNode).closest('.dropdown')[0];
        }
      }
      console.debug("target is %o", targetNode);

      var isActive = domClass.contains(targetNode, 'open');
      
      clearMenus();

      if(!isActive){
        domClass.add(targetNode, 'open');
      }
    }
  });

  
  function clearMenus(e){
    //var p = query(toggle).parent();
    var p = query('.dropdown-toggle').parent();
    //console.debug("in (global) clearMenus; e=%o", e);
    p.removeClass('open');
  }
  
  
  function toggler(e){
    var node = e.target;
    var obj = NodePersist.loadFromNode(node, 'dropdown');
    if(!obj){
      obj = new Dropdown(node, {});
      NodePersist.saveToNode(obj, node, 'dropdown');
    }
    obj.toggle();
    
    e.preventDefault();
    e.stopPropagation();
    
    return false;
  }

  /* DROPDOWN PLUGIN DEFINITION 
   * ========================== */

   lang.extend(query.NodeList, {
     dropdown: function(option){

       return this.forEach(function(node){

         var options = (typeof option == 'object') ? option : options

         var obj = NodePersist.loadFromNode(node, 'dropdown');
         if(!obj){
           obj = new Dropdown(node, options);
           NodePersist.saveToNode(obj, node, 'dropdown');
         }else{
           lang.mixin(obj, options)
         }

         if(typeof option == 'string'){
           obj[option]();
         }

       });

     }
   });


   /* DROPDOWN DATA-API
    * ================= */

  ready(function () {
    query('html').on('click', clearMenus);
    on(win.body(), on.selector(toggle, 'click'), toggler);
  });

  return Dropdown;

});
  