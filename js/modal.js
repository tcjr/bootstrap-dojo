define([
  './support/with-node-data',
  './support/node-persist',
  './support/transition',
  'dojo/ready',
  'dojo/_base/window',
  'dojo/on',
  'dojo/query',
  'dojo/_base/lang',
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/NodeList-data',
  'dojo/NodeList-dom',
  'dojo/NodeList-manipulate',
  'dojo/NodeList-traverse'
],
function(_WithNodeData, NodePersist, trans, ready, win, on, query, lang, declare, domAttr, domClass, domStyle, domConstruct){
  

  var Modal = declare([_WithNodeData], {

    backdrop: true,
    keyboard: true,
    //show: true,
    
    constructor: function(content, options){
      
      this.domNode = domConstruct.create(content);

      on(this.domNode, '[data-dismiss="modal"]:click', lang.hitch(this, 'hide'));
    },
    
    toggle: function(){
      console.debug("TOGGLE");
      return this[!this.isShown ? 'show' : 'hide']()
    },
    
    show: function(){
      console.debug("SHOW")
      if(this.isShown){ return }
      
      domClass.add(win.body(), 'modal-open')
      this.isShown = true;
      //on.emit(this.domNode, 'show')
      
      //this.escape();
      this._backdrop(lang.hitch(this, function(){
        // called when backdrop is done animating and ready to go
        console.debug("callback called!");
        var transition = trans && domClass.contains(this.domNode, 'fade');
        
        domConstruct.place(this.domNode, win.body()) // TODO: only do this if it is not already a child of body
        
        domStyle.set(this.domNode, 'display', 'block');
        
        if(transition){ this.domNode.offsetWidth }
        
        domClass.add(this.domNode, 'in');
        
        // if(transition){
        // }else{
        // }
        
      }));
    },
    
    _backdrop: function(callback){
      console.debug("_backdrop");
      var animate = domClass.contains(this.domNode, 'fade') ? 'fade' : '';
      
      if(this.isShown && this.backdrop){
        var doAnimate = trans && animate;
        
        this.backdropNode = domConstruct.place('<div class="modal-backdrop ' + animate + '" />', win.body());
        
        if(this.backdrop != 'static'){
          on(this.backdropNode, 'click', lang.hitch(this, 'hide'));
        }
        
        if(doAnimate){ this.backdropNode.offsetWidth; }
        
        domClass.add(this.backdropNode, 'in');
        
        if(doAnimate){
          on.once(this.backdropNode, trans.end, callback);
        }else{
          callback()
        }
        
      }else if(!this.isShown && this.backdropNode){
        domClass.remove(this.backdropNode, 'in');
        
        if(trans && domClass.contains(this.domNode, 'fade')){
          on.once(this.backdropNode, trans.end, lang.hitch(this, '_removeBackdrop'))
        }else{
          this._removeBackdrop();
        }
        
      }else if(callback){
        callback();
      }
    },
    
    _removeBackdrop: function(){
      console.debug("_removeBackdrop");
      domConstruct.destroy(this.backdropNode);
      this.backdropNode = null;
    },
    
    
    hide: function(e){
      if(e){ e.preventDefault(); }
      console.debug("HIDE this.domNode=%o", this.domNode);
      
      if(!this.isShown){ return }
      
      this.isShown = false;
      domClass.add(win.body(), 'modal-open')
      //on.emit(this.domNode, 'hide')
      domClass.remove(this.domNode, 'in');
      
      if(trans && domClass.contains(this.domNode, 'fade')){
        this._hideWithTransition();
      }else{
        this._hideModal();
      }
    },
    
    _hideModal: function(){
      domStyle.set(this.domNode, 'display', 'none');
      //on.emit(this.domNode, 'hidden')
      this._backdrop();
    },
    
    _hideWithTransition: function(){
      var eh;
      var timeout = setTimeout(lang.hitch(this, function(){
        if(eh && eh.remove){
          eh.remove();
          this._hideModal();
        }
      }), 500);
      
      eh = on.once(this.domNode, trans.end, lang.hitch(this, function(){
        clearTimeout(timeout);
        this._hideModal();
      }));
    }
    
  });


  /* MODAL PLUGIN DEFINITION
   * ======================= */

  // FN.modal = function(option){
  //   // this ctx is the NodeList
  //   
  //   return this.forEach(function(node){
  //     
  //   });
  // };

  // This is the general attach funciton.  It is basically a load-from-node-or-create function.
  // TODO: figure out how to make a version of this that will work for all the components.
  // Something like this???:  Modal.attach = attacherFunction(Modal, 'modal')
  Modal.attach = function(node, options){
    console.debug("attaching to %o", node)
    options = options || {};
    var obj = NodePersist.loadFromNode(node, 'modal');
    if(!obj){
      obj = new Modal(node, options);
      NodePersist.saveToNode(obj, node, 'modal');
    }else{
      lang.mixin(obj, options)
    }
    console.debug("returning %o", obj);
    
    return obj;
  };


  ready(function () {
    // This connects to the modal launcher, not the modal itself.  The actual modal is specified in either data-target or href attributes.
    on(win.body(), on.selector('[data-toggle="modal"]', 'click'), function(e){
      console.debug("e = %o", e)
      var option = 'toggle'; // FIXME
      var target = domAttr.get(e.target, 'data-target') || domAttr.get(e.target, 'href');
      target = query(target)[0]; // FIXME
      
      Modal.attach(target)[option]();
      
      e.preventDefault();
      
      
    });
  });

  return Modal;

});
  