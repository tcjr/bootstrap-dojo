define([
  './support/node-persist',
  'dojo/_base/declare',
  'dojo/ready',
  'dojo/query',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/dom-construct',
  './tooltip'
],
function(NodePersist, declare, ready, query, lang, domAttr, domClass, domConstruct, Tooltip){

  /* POPOVER PUBLIC CLASS DEFINITION
   * =============================== */
  var Popover = declare([Tooltip], {
    placement: 'right',
    content: '',
    template: '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>',
    
    constructor: function(element, options){
      this.init(element, options);
    },
    
    setContent: function(){
      var tipNode = this.tip(),
          title = this.getTitle(),
          content = this.getContent();
          
      query('.popover-title', tipNode)[ lang.isObject(title) ? 'append' : 'html' ](title); // TODO: verify NodeList.append works
      query('.popover-content > *', tipNode)[ lang.isObject(content) ? 'append' : 'html' ](content);
      
      domClass.remove(tipNode, 'fade top bottom left right in');
    },
    
    hasContent: function(){
      return this.getTitle() || this.getContent();
    },
    
    getContent: function(){
      var content = this.getNodeData('content')
        || (typeof this.content == 'function' ? this.content.call(this.domNode) : this.content);
      
      content = content.toString().replace(/(^\s*|\s*$)/, "");
      return content;  
    },
    
    tip: function(){
      if(!this.tipNode){
        this.tipNode = domConstruct.toDom(this.template);
      }
      return this.tipNode;
    }
    
  }); 
    
  
  /* POPOVER PLUGIN DEFINITION
   * ========================= */

   lang.extend(query.NodeList, {
     popover: function(option){

       return this.forEach(function(node){
         var options = (typeof option == 'object') ? option : options

         var obj = NodePersist.loadFromNode(node, 'popover');
         if(!obj){
           obj = new Popover(node, options);
           NodePersist.saveToNode(obj, node, 'popover');
         }
         lang.mixin(obj, options)

         if(typeof option == 'string'){
           obj[option]();
         }
       });
     }
   });
      
  return Popover;
});
