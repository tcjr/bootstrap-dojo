define([
  './support/with-node-data',
  './support/node-persist',
  './support/transition',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/ready',
  'dojo/_base/window',
  'dojo/on',
  'dojo/query',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/dom-construct',
  'dojo/NodeList-traverse',
  'dojo/NodeList-dom',
  'dojo/NodeList-manipulate'
],
function(_WithNodeData, NodePersist, trans, declare, lang, ready, win, on, query, domAttr, domClass, domConstruct){

  var dismiss = '[data-dismiss="alert"]';

  /* ALERT CLASS DEFINITION
   * ====================== */  
  var Alert = declare([_WithNodeData], {
    constructor: function(element, options){
      console.debug("CTOR; this=%o, element=%o", this, element);
      this.domNode = element;
      lang.mixin(this, options);
    },
    
    close: function(){
      console.debug("close called; args=%o; this=%o", arguments, this)
      on.emit(this.domNode, 'close', {bubbles:true, cancelable:true});
      domClass.remove(this.domNode, 'in');

      if(domClass.contains(this.domNode, 'fade')){
        on(this.domNode, trans.end, lang.hitch(this, 'removeElement'));
      }else{
        this.removeElement();
      }
    },
    
    removeElement: function(){
      console.debug("removing element")
      console.debug(".. emitting event; node=%o", this.domNode)
      on.emit(this.domNode, 'closed', {bubbles:true, cancelable:true});
      console.debug(".. calling destroy")
      domConstruct.destroy(this.domNode);
    }
    
  });
  

  function closer(e){
    console.debug("closer(); args = %o, this = %o", arguments, this);
    var alertNode = query(e.target).closest('.alert')[0];
    // NOTE: the persist stuff is probably not necessary here since alerts are single use and get removed
    console.debug("alertNode is %o", alertNode)
    var obj = NodePersist.loadFromNode(alertNode, 'alert');
    if(!obj){
      obj = new Alert(alertNode, {});
      NodePersist.saveToNode(obj, alertNode, 'alert');
    }
    obj.close();

    if(e){ e.preventDefault(); }
  }

  /* ALERT PLUGIN DEFINITION 
   * ======================= */
   lang.extend(query.NodeList, {
     alert: function(option){

       return this.forEach(function(node){

         var options = (typeof option == 'object') ? option : options

         var obj = NodePersist.loadFromNode(node, 'alert');
         if(!obj){
           obj = new Alert(node, options);
           NodePersist.saveToNode(obj, node, 'alert');
         }else{
           lang.mixin(obj, options)
         }

         if(typeof option == 'string'){
           obj[option]();
         }

       });

     }
   });




  ready(function () {
    on(win.body(), on.selector(dismiss, 'click'), closer);
  });

  return Alert;
});
  