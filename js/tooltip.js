define([
  './support/with-node-data',
  './support/node-persist',
  'dojo/_base/declare',
  'dojo/ready',
  'dojo/_base/window',
  'dojo/on',
  'dojo/mouse',
  'dojo/query',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/dom-geometry',
  'dojo/NodeList-dom',
  'dojo/NodeList-manipulate',
  'dojo/NodeList-traverse'
],
function(_WithNodeData, NodePersist, declare, ready, win, on, mouse, query, lang, domAttr, domConstruct, domClass, domStyle, domGeom){

  /* TOOLTIP PUBLIC CLASS DEFINITION
   * =============================== */
   
   var Tooltip = declare([_WithNodeData], {
     animation: true,
     delay: 0,
     selector: false,
     placement: 'top',
     trigger: 'hover',
     title: '',
     template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
     type: 'tooltip',
     
     constructor: function(element, options){
       this.init(element, options);
     },
     
     init: function(element, options){
       //console.debug("init; args=%o", arguments)
       var eventIn, eventOut;
       this.domNode = element;
       this.initOptions(options);
       this.enabled = true;

       if(this.trigger != 'manual'){
         eventIn  = this.trigger == 'hover' ? mouse.enter : 'focus';
         eventOut = this.trigger == 'hover' ? mouse.leave : 'blur';

         if(this.selector){
           eventIn = on.selector(this.selector, eventIn);
           eventOut = on.selector(this.selector, eventOut);
         }
         //console.debug("setting up triggers (%o, %o) on %o", eventIn, eventOut, this.$element)
         on(this.domNode, eventIn, lang.hitch(this, 'enter'));
         on(this.domNode, eventOut, lang.hitch(this, 'leave'));

         // TODO: selector support
         this.fixTitle();
       }
     },
     
     initOptions: function(opts){
       lang.mixin(this, opts || {});
       if(('delay' in this) && (typeof this.delay == 'number')){
         this.delay = {
           show: this.delay,
           hide: this.delay
         };
       }
     },
     
     enter: function(e){
       // console.debug("ENTER, this=%o", this)
       if(!this.delay || !this.delay.show){
         this.show();
       }else{
         this.hoverState = 'in';
         setTimeout(lang.hitch(this, function(){
           if(this.hoverState=='in'){
             this.show();
           }
         }), this.delay.show)
       }
     },
     
     leave: function(){
       // console.debug("LEAVE")
       if(!this.delay || !this.delay.hide){
         this.hide();
       }else{
         this.hoverState = 'out';
         setTimeout(lang.hitch(this,function(){
           if(this.hoverState=='out'){
             this.hide();
           }
         }), this.delay.hide)
       }
     },
     
     show: function(){
       var tipNode, inside, pos, actualWidth, actualHeight, placement, tp;

       if(this.hasContent() && this.enabled){
         tipNode = this.tip();
         this.setContent();

         if(this.animation){
           domClass.add(tipNode, 'fade');
         }

         placement = typeof this.placement == 'function' ?
           this.placement.call(this, tipNode, this.domNode) :
           this.placement;

         inside = /in/.test(placement);

         domConstruct.place(tipNode, (inside ? this.domNode : win.body()), 'last');
         domStyle.set(tipNode, {top:0, left:0, display:'block'});

         pos = this.getPosition(inside);

         var tipPos = domGeom.position(tipNode);
         actualWidth = tipPos.w;
         actualHeight = tipPos.h;

         switch(inside ? placement.split(' ')[1] : placement){
           case 'top':
             tp = {top: (pos.top - actualHeight)+'px', left: (pos.left + pos.width / 2 - actualWidth / 2)+'px'}
           break;
           case 'bottom':
             tp = {top: (pos.top + pos.height)+'px', left:( pos.left + pos.width / 2 - actualWidth / 2)+'px'}
             break
           case 'left':
             tp = {top: (pos.top + pos.height / 2 - actualHeight / 2)+'px', left: (pos.left - actualWidth)+'px'}
             break
           case 'right':
             tp = {top: (pos.top + pos.height / 2 - actualHeight / 2)+'px', left: (pos.left + pos.width)+'px'}
             break          
         }
         domStyle.set(tipNode, tp);
         domClass.add(tipNode, placement);
         domClass.add(tipNode, 'in');

       }
     },
     
     setContent: function(){
       var tipNode = this.tip();
       query('.tooltip-inner', tipNode).html(this.getTitle());
       domClass.remove(tipNode, 'fade in top bottom left right');
     },
     
     hide: function(){
       var tipNode = this.tip();
       domClass.remove(tipNode, 'in');
       // TODO: add animation/transition
       domConstruct.destroy(tipNode);
     },
     
     fixTitle: function(){
       var elem = this.domNode;
       var t = domAttr.get(elem, 'title');
       if(t || typeof(this.getNodeData('original-title')) != 'string' ){
         this.setNodeData('original-title', t || '');
         domAttr.remove(elem, 'title');
       }
     },
     
     hasContent: function(){
       return this.getTitle();
     },
     
     getPosition: function(inside){
       var mb = domGeom.position(this.domNode, true)
       var p = {};
       lang.mixin(p, inside ? {top: 0, left: 0} : {top: mb.y, left: mb.x});
       lang.mixin(p, {
         width: mb.w,
         height: mb.h
         // width: this.$element[0].offsetWidth,
         // height: this.$element[0].offsetHeight
       });

       return p;
     },
     
     getTitle: function(){
       var title = this.getNodeData('original-title')
         || (typeof this.title == 'function' ? this.title.call(this.domNode) : this.title );

       title = (title || '').toString().replace(/(^\s*|\s*$)/, "")
       return title;
     },
     
     tip: function() {
       return this.tipNode = this.tipNode || domConstruct.toDom(this.template);
     },
     
     validate: function(){
     },
     
     enable: function(){
       this.enabled = true;
     },
     
     disable: function(){
       this.enabled = false;
     },
     
     toggleEnabled: function(){
       this.enabled = !this.enabled;
     },
     
     toggle: function(){
       this[domClass.contains(this.tip(), 'in') ? 'hide' : 'show']();
     }
   });
  
  
  /* TOOLTIP PLUGIN DEFINITION
   * ========================= */

   lang.extend(query.NodeList, {
     tooltip: function(option){

       return this.forEach(function(node){
         var options = (typeof option == 'object') ? option : options

         var obj = NodePersist.loadFromNode(node, 'tooltip');
         if(!obj){
           obj = new Tooltip(node, options);
           NodePersist.saveToNode(obj, node, 'tooltip');
         }
         lang.mixin(obj, options)

         if(typeof option == 'string'){
           obj[option]();
         }
       });
     }
   });

   return Tooltip;
      
});
