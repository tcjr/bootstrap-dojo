var dojoConfig = {
  async: true,
  packages: [{
    name: 'bs',
    location: location.pathname.replace(/\/[^/]+$/, '../..') + '/js'
  }]
}
