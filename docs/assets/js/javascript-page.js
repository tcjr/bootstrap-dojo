// NOTICE!! DO NOT USE ANY OF THIS JAVASCRIPT
// IT'S ALL JUST JUNK FOR OUR DOCS!
// ++++++++++++++++++++++++++++++++++++++++++
require(['dojo/query', 'dojo/ready', 'dojo/dom-geometry', 'dojo/on', 'dojo/dom-class', 'bs'], 
function(query, ready, geom, on, domClass){

  ready(function(){

    // fix sub nav on scroll
    var nav = query('.subnav');
    if(nav.length){
      nav = nav[0];
      var navTop = geom.position(nav, true).y - 40;
      var isFixed = 0;

      processScroll();
      
      on(document, 'scroll', processScroll);

      function processScroll(){
        var i, scrollTop = -(geom.position(document.body, false).y);
        if (scrollTop >= navTop && !isFixed) {
          isFixed = 1;
          domClass.add(nav, 'subnav-fixed');
        } else if (scrollTop <= navTop && isFixed) {
          isFixed = 0
          domClass.remove(nav, 'subnav-fixed');
        }
      }
    }
    
    
    // Disable certain links in docs
    query('section [href^=#]').on('click', function (e) {
      e.preventDefault()
    })

    // make code pretty
    if(window.prettyPrint) { prettyPrint() }

    // tooltip demo
    query('.tooltip-demo.well a[rel=tooltip]').tooltip();

    query('.tooltip-test').tooltip() // in modal
    query('.popover-test').popover() // in modal
    
    // popover demo
    query("a[rel=popover]")
      .popover()
      .on('click', function(e){
        e.preventDefault()
      })

    // button state demo
    query('#fat-btn').on('click', function(e){
      var btn = query(e.target)
      btn.button('loading')
      setTimeout(function () {
        btn.button('reset')
      }, 3000)
      
    });

    // carousel demo
    try { 
      query('#myCarousel').carousel()
    }catch(e){
      console.log("carousel not yet implemented")
    }

  });

});
