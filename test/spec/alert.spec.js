define(['dojo/query', 'dojo/dom-attr', 'dojo/dom-construct', 'dojo/dom', 'dojo/on', 'bs/alert'], 
function(query, domAttr, domConstruct, dom, on){
  
  describe("Alert", function(){

    // describe("basics", function(){
    // 
    //   it("should be defined on NodeList object", function(){
    //     expect(query(document.body).alert).toBeDefined();
    //   })
    // 
    //   it("should return element", function(){
    //     expect(query(document.body).alert()[0]).toEqual(document.body)
    //   })
    //   
    // })
    
    it("should remove element when clicking .close", function(){
      var f = dom.byId('unit-fixture');
      expect(f).toBeTruthy();
    
      var alertHTML = '<div class="alert alert-error">'
        + '<a class="close" href="#" data-dismiss="alert">×</a>'
        + '<p><strong>Holy guacamole!</strong> Best check yo self, you\'re not looking too good.</p>'
        + '</div>';
        
      domConstruct.place(alertHTML, f);
      expect(query('.alert', f).length).toBe(1);
      var alert = query('.alert', f).alert();
    
      // trigger click event
      var closeNode = query('.close', f)[0];
      on.emit(closeNode, "click", {bubbles: true, cancelable:true});
      
      expect(query('.alert', f).length).toBe(0);
      expect(dom.byId('unit-fixture')).toBeTruthy(); // test to make sure fixture wasn't removed
    })

    it("should asynchronously remove element when clicking .close", function(){
      var f = dom.byId('unit-fixture');
      expect(f).toBeTruthy();

      var alertHTML = '<div class="alert alert-error fade in">'
        + '<a class="close" href="#" data-dismiss="alert">×</a>'
        + '<p><strong>Holy guacamole!</strong> Best check yo self, you\'re not looking too good.</p>'
        + '</div>';
        
      domConstruct.place(alertHTML, f);

      var closedRecieved = false;

      runs(function(){
        
        expect(query('.alert', f).length).toBe(1);
        var alertNode = query('.alert', f);
        alertNode.alert();
        
        on(alertNode, 'close', function(){ console.debug("close handler"); });
        on(alertNode, 'closed', function(){ console.debug("closed handler"); closedRecieved = true; });
        
        // trigger click event
        var closeNode = query('.close', f)[0];
        on.emit(closeNode, "click", {bubbles: true, cancelable:true});

      })

      waitsFor(function(){
        return closedRecieved;
      }, "closed event never triggered", 1000)
      
      runs(function(){
        expect(query('.alert', f).length).toBe(0);
        expect(dom.byId('unit-fixture')).toBeTruthy(); // test to make sure fixture wasn't removed
      })
      
    })
    

  });
  
});
