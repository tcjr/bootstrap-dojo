define(['dojo/query', 'bs/popover'], 
function(query){
  
  describe("Popover", function(){

    it("should be defined on NodeList object", function(){
      expect(query(document.body).popover).toBeDefined();
    })

  });
  
});
