define(['dojo/query', 'dojo/dom-attr', 'dojo/dom-construct', 'bs/button'], 
function(query, domAttr, domConstruct){
  
  describe("Button", function(){

    it("should be defined on NodeList object", function(){
      expect(query(document.body).button).toBeDefined();
    })

    it("should return element", function(){
      expect(query(document.body).button()[0]).toEqual(document.body)
    })
    
    it("should use data-loading-text for loading state", function(){
      var btn = domConstruct.toDom('<button class="btn" data-loading-text="fat">mdo</button>');
      expect(btn.innerHTML).toEqual('mdo');
      query(btn).button('loading');
      // This fails because dojo's implemention of NodeList.data() does not support looking 
      // at the html5 data-* attributes.
      expect(btn.innerHTML).toEqual('fat');
    })

    it("should set button disabled in loading state", function(){
      var btn = domConstruct.toDom('<button class="btn">tc</button>');
      expect( domAttr.get(btn, 'disabled') ).toBeFalsy()
      
      query(btn).button('loading');
      
      waits(1);
      runs(function(){
        expect( domAttr.get(btn, 'disabled') ).toBeTruthy()
      });
    })

  });
  
});
