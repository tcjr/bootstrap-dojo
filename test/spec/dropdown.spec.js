define(['dojo/query', 'bs/dropdown'], 
function(query){
  
  describe("Dropdown", function(){

    it("should be defined on NodeList object", function(){
      expect(query(document.body).dropdown).toBeDefined();
    })

  });
  
});
