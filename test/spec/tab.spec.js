define(['dojo/query', 'bs/tab'], 
function(query){
  
  describe("Tab", function(){

    it("should be defined on NodeList object", function(){
      expect(query(document.body).tab).toBeDefined();
    })

  });
  
});
