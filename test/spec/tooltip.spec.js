define(['dojo/query', 'bs/tooltip'], 
function(query){
  
  describe("Tooltip", function(){

    it("should be defined on NodeList object", function(){
      expect(query(document.body).tooltip).toBeDefined();
    })

  });
  
});
