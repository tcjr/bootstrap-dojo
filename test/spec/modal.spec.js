define(['dojo/query', 'dojo/dom-attr', 'dojo/dom-construct', 'bs/modal'], 
function(query, domAttr, domConstruct){
  
  describe("Modal", function(){

    it("should be defined on NodeList object", function(){
      expect(query(document.body).modal).toBeDefined();
    })

  });
  
});
