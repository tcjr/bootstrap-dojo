# Bootstrap and Dojo

Twitter's Bootstrap UI library is great.  It includes a Less/CSS library and a jQuery-based Javascript plugin interaction library. I wanted to use Bootstrap with a Dojo application I was developing.  So, I ported some of the bootstrap javascript to Dojo 1.7+.

So far, I've implemented *alert*, *button*, *dropdown*, *popover*, *tab*, and *tooltip*.

I'm striving to have the declarative syntax ("data api") be 100% functionally compatible with the original jQuery version.

NOTES SO FAR (to myself):
=========================

Philosophy
----------

Initially, this is a pretty straight-ahead port.  As I port it, though, I think it would be preferable to implement it a little more Dojo-like.  However, it is essential that it keeps with the Bootstrap philosophy (as described in the original readme).  I want to make sure the markup-only approach always "just works." 


Single node vs NodeList
-----------------------

jQuery seamlessly operates on single nodes and lists of nodes using the same functions.  Everything essentially gets "wrapped" with the jQuery object.  It is not uncommon, for example, to use an expression like this:

    var element = $('#some-id');
    element.addClass('myClass');
    
How you would do this in Dojo often depends on how it will be used.  Both of these would work in Dojo:

    var element = query('#some-id');
    element.addClass('myClass');
    
or

    var element = dom.byId('some-id');
    domClass.add('myClass');

I know it is perfectly valid, but to me, it feels a little funny to use the query/NodeList version in Dojo when you are working with a single node.


Namespaced Events
-----------------

jQuery supports namespacing event names, and Bootstrap takes advantage of that.  I don't really know if Dojo has a similar facility or not.


Opt-in / Opt-out API
--------------------

The API has the ability to "opt-out" by unregistering the event handlers that are automatically added.  I think this is a good opportunity for the AMD module to be utilized.  So, instead of doing this jQuery code:

    // jQuery
    $('body').off('.alert.data-api');
    // ...
    
You could do something like this:

    // Dojo
    require(['bs/alert'], function(Alert){
      Alert.optOut();
      // ...
    });

And, for the components where the API is opt-in, it could provide a simple interface:

    // jQuery
    $('[rel="tooltip"]').tooltip();
    
    // Dojo
    Toolip.optIn()

In both of these cases, the details of the selectors are hidden.  (I'm not sure this is a big advantage.)

